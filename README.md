[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/rnd-share/FirstGitpod) 

# FirstGitpod

Start coding in a [ready-to-code development environment](https://www.gitpod.io):


<a href="https://gitpod.io/#https://gitlab.com/rnd-share/FirstGitpod" style="padding: 10px;">
    <img src="https://gitpod.io/button/open-in-gitpod.svg" width="150" alt="Push" align="center">
</a>


## Step By Step
1. Permission File `chmod +x .`
2. Build `./gradlew clean build`
3. Run `./gradlew bootRun`
4. Klik menu view - Open port  
    <img src="https://gitlab.com/rnd-share/FirstGitpod/-/raw/master/Run.png" width="150" alt="Push" align="center"> 
4. Open URL /api/hello

